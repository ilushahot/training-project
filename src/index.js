import './scss/main.scss';
import { Swiper, Controller, Pagination, Navigation } from 'swiper';

Swiper.use([Controller, Pagination, Navigation]);

// fix custom event for IE11
(function () {
    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        const evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }
    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

// ============ СЛАЙДЕРЫ ============

let imgArr = ['Rostov-on-Don, Admiral', 'Sochi Thieves', 'Rostov-on-Don Patriotic'];
let sliderImg = new Swiper(".slider__img", {
    navigation: {
        nextEl: ".swiper-button-img"
    },
    pagination: {
        el: ".swiper-pagination-img",
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (imgArr[index]) + '</span>';
        },
    },
    slidesPerView: 1.2,
    spaceBetween: 0,
    loop: true,
    simulateTouch: false,
})

let sliderImgMobile = new Swiper(".slider__img-mobile", {
    navigation: {
        nextEl: ".swiper-button-img-mobile-next",
        prevEl: ".swiper-button-img-mobile-prev"
    },
    loop: true,
})

let sliderData = new Swiper(".slider__data", {
    navigation: {
        nextEl: ".navigation__arrow-right",
        prevEl: ".navigation__arrow-left"
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
        renderBullet: function (index, className) {
            return '<button class="' + className + '">' + '</button>';
        },
    },
    slidesPerView: 1.2,
    spaceBetween: 120,
    loop: true,
    simulateTouch: false,
})

let sliderDataLower = new Swiper(".slider__data-lower", {
    loop: true,
    slidesPerView: 1,
    controller: {
        control: sliderData
    }
})

let realizeData = new Swiper(".slider__realize", {
    navigation: {
        nextEl: ".realize__slides-nav-mob-right",
        prevEl: ".realize__slides-nav-mob-left"
    },
    slidesPerView: 1,
    loop: true,
    autoHeight: true
})

let designStepsSlider = new Swiper('.design__steps-slider', {
    navigation: {
        nextEl: ".slider-steps-next",
        prevEl: ".slider-steps-prev"
    },
    pagination: {
        el: ".design__slider-nav",
        clickable: true,
        type: "custom",
        bulletClass: "design__slider-point"
    },
    loop: true,
})
let designDescSlider = new Swiper('.design__desc-slider', {
    centeredSlidesBounds: true,
    centeredSlides: true,
    loop: true
})
designStepsSlider.controller.control = designDescSlider
designDescSlider.controller.control = designStepsSlider
sliderImg.controller.control = sliderData
sliderData.controller.control = sliderImg;
sliderImgMobile.controller.control = sliderDataLower
sliderDataLower.controller.control = sliderImgMobile

// ============ СЛАЙДЕРЫ ============

// HAMBURGER

const menuIcon = document.querySelector('.hamburger__menu-icon');
document.getElementById("trigger").onclick = function () { open() };

function toggleMenuIcon() {
    menuIcon.classList.toggle('active')
}
function open() {
    document.getElementById("menu").classList.toggle("show");
}
menuIcon.addEventListener('click', toggleMenuIcon);


// VIDEO
let playBtn = document.getElementById('play-btn');
let vidContainer = document.getElementById('video-container');
let vid = document.getElementById('video')

vidContainer.addEventListener("click", play)
vid.addEventListener("click", play)

function play() {
    if (vid.paused) {
        vid.play();
        playBtn.classList.add("o");
    }
    else {
        playBtn.classList.remove("o");
    }
}

const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault()

        const blockID = anchor.getAttribute('href').substr(1)

        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        })
    })
}